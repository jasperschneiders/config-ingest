# Configs for Ingest nodes

#### http_port = 8333
The port used for HTTP-Streamingthis is not configured on a per stream basis as the streams are accessed by path

#### allowed_country_codes = ch,de,at
global geo blocking config, clients from the given countries will be allowed if not explicitly denied in a blacklist. This setting can be overwritten in the stream configuration

#### geoblocking_update_providers = http://hz-cc.hbbtvlive.de/geoblocking.json
URL to JSON file containing initial values and updates for the geoblocking engine. More than one URL can be given, separated by a comma.

The geo config looks like this (i.e. one dictionary per stream/channel):
{
    "kanal1":{
        "blacklist": ["173.194.113.103", "173.194.113.104"],
        "whitelist": ["127.0.0.1"]
    },
    "kanal2":{
        "blacklist": ["173.194.113.104"],
        "whitelist": []
    }
}

#### debug = false
turn debug logging on

#### syslog_logging_url = udp://192.168.37.63:20516
URL to the syslog service

#### dsd_log_url = http://logcollector.gl-systemhaus.de/logme
URL used to send connection logs to

#### proxy_internal_http_endpoints = []
http endpoints exposed by the master server that should be relayed on the main server port and not only on the internal health url (one of ['/health', '/count', '/free', '/load', '/suicide', '/version'])

#### internal_health_port = 8743
port the master process will listen on for health requests. endpoints exposed on this port can be proxied to the main server port by using the setting proxy_internal_http_endpoints explained above

#### connection_log_interval = 10
specifies how often connection data (for each connection) should be logged (in SECONDS), default 300 s

#### geoip_location =
specifies the absolute path to GeoIP.dat (NOTE: in a Docker container this is the path inside the container)if not given configs/geoip/GeoIP.dat inside node's root folder is used

#### BAD_USER_AGENTS[] = Humax\;iCord
specifies User-Agent or strings included in the User-Agent to exclude them from delivery

Configuration for single streams. Each section has to start with 'streams.' as this puts all stream configs in a list to be used in the server:
```
[streams.mdr_event1]

sources[] = 192.168.37.121:20001
sources[] = 192.168.37.122:20001
; list of source streams, all entries "sources[]" will be converged into a single list

destination[] = http:/mdr/event1
; list of destinations, all entries "destination[]" will be converged into a single list the value of each entry is split on the colon (':'), then the first part will denote the playout protocol (http or tcp) and the second part the path (http) and the port (tcp), respectively.

geo = off
; turn geoblocking on or off for this stream

push = false
; controls ingest (push or pull) (UDP or TCP, respectively)

geoblock_redirect =
; specifies the URL a geoblocked client is sent to, e.g. for a "sorry, you're not allowed to watch" stream

geoblock_mimetype =
; specify the mimetype of the media served by geoblock_redirect (default: video/mpeg)

allowed_country_codes =
; specifies which country are allowed to pull the content, comma-separated, overwrites global setting
```